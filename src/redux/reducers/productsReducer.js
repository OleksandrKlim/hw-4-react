const defaultState = {
  products: [],
  isLoading: false,

};

export function cardReducer(state = defaultState, action) {
  switch (action.type) {
    case "GET_CARD_REQUEST":
      return { ...state, isLoading: true };
    case "GET_CARD_SUCCESS":
      return {
        ...state,
        products: action.payload.products,
        isLoading: false,
      };
    case "GET_CARD_ERROR":
      return { ...state, isLoading: false };
   
    default:
      return state;
  }
}
