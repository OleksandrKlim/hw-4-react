import { getFavoriteToLocalStorage, getBasketToLocalStorage } from "../actions";
export const productsCardThunk = () => {
  return function (dispatch) {
    dispatch({ type: "GET_CARD_REQUEST" });

    fetch("./product/data/data.json")
      .then((r) => r.json())
      .then((products) => {
        dispatch({ type: "GET_CARD_SUCCESS", payload: { products } });
      })
      .catch((error) => {
        dispatch({ type: "GET_CARD_ERROR" });
        alert(error);
      });
  };
};
export function saveLikeToLocalStorage(data) {
  return function (dispatch) {
    localStorage.setItem("Like", JSON.stringify(data));
  };
}
export function getLikeLocalStorage() {
  return function (dispatch) {
    const data = localStorage.getItem("Like");
    data && dispatch(getFavoriteToLocalStorage(JSON.parse(data)));
  };
}
export function saveBasketToLocalStorage(data) {
  return function (dispatch) {
    localStorage.setItem("Basket", JSON.stringify(data));
  };
}
export function getBasketLocalStorage() {
  return function (dispatch) {
    const data = localStorage.getItem("Basket");

    data && dispatch(getBasketToLocalStorage(JSON.parse(data)));
  };
}
