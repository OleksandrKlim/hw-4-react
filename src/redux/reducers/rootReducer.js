import { combineReducers } from "redux";
import { basketReducer } from "./basketReducer";
import { favoritesReducer } from "./favoritesReducer";
import { cardReducer } from "./productsReducer";
import { modalReducer } from "./modalReducer";
import { idReducer } from "./productIdReducer";
export const rootReducer = combineReducers({
  basket: basketReducer,
  favorites: favoritesReducer,
  products: cardReducer,
  modal: modalReducer,
  selectedCard : idReducer,
});
