import {
  ADD_TO_BASKET_ACTION_TYPE,
  ADD_TO_BASKET_VALUE_ACTION_TYPE,
  REMOVE_FROM_BASKET_ACTION_TYPE,
  REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
  SAVE_BASKET_TO_LOCAL_STORAGE,
  GET_BASKET_LOCAL_STORAGE,
} from "../actions";
const basket = {}
export function basketReducer(state = basket, action) {
  switch (action.type) {
    case ADD_TO_BASKET_ACTION_TYPE:
      return { ...state, [action.payload]: (state[action.payload] = 1) };
    case ADD_TO_BASKET_VALUE_ACTION_TYPE:
      return { ...state, [action.payload]: state[action.payload] + 1 };
    case REMOVE_VALUE_FROM_BASKET_ACTION_TYPE:
      return {
        ...state,
        [action.payload]: state[action.payload] - 1,
      };
    case REMOVE_FROM_BASKET_ACTION_TYPE:
      const key = action.payload;
      const newState = { ...state };
      delete newState[key];
      return newState;
    case SAVE_BASKET_TO_LOCAL_STORAGE:
      return { ...state };
    case GET_BASKET_LOCAL_STORAGE:
      return action.payload ;
    default:
      return state;
  }
}
