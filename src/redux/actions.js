export const ADD_PRODUCT_TO_FAVORITES_ACTION_TYPE = "favorites/addProduct";
export const DEL_PRODUCT_FROM_FAVORITES_ACTION_TYPE = "favorites/delProduct";

export const ADD_TO_BASKET_ACTION_TYPE = "basket/addToBasket";
export const ADD_TO_BASKET_VALUE_ACTION_TYPE = "basket/adProductValue";
export const REMOVE_FROM_BASKET_ACTION_TYPE = "basket/removeFromBasket";
export const REMOVE_VALUE_FROM_BASKET_ACTION_TYPE =
  "basket/decrementProductValue";

export const MODAL_FIRST_VISIBLE_ACTION_TYPE = "modal/modalFirstVisible";
export const MODAL_FIRST_INVISIBLE_ACTION_TYPE = "modal/modalFirstInvisible ";
export const MODAL_SECOND_VISIBLE_ACTION_TYPE = "modal/modalSecondVisible ";
export const MODAL_SECOND_INVISIBLE_ACTION_TYPE = "modal/modalSecondInvisible ";
export const MODAL_THERE_VISIBLE_ACTION_TYPE = "modal/modalThereVisible ";
export const MODAL_THERE_INVISIBLE_ACTION_TYPE = "modal/modalThereInvisible ";

export const GET_STATE_ARTICLE = "products/addIdToState";
export const CLEAR_STATE_ARTICLE = "products/clearIdState";

export const SAVE_FAVORITES_TO_LOCAL_STORAGE = "storage/setLike";
export const GET_FAVORITES_LOCAL_STORAGE = "storage/getLike";
export const SAVE_BASKET_TO_LOCAL_STORAGE = "storage/setBasket";
export const GET_BASKET_LOCAL_STORAGE = "storage/getBasket";

export const addProductToFavorites = (productId) => ({
  type: ADD_PRODUCT_TO_FAVORITES_ACTION_TYPE,
  payload: { productId },
});
export const delProductFromFavorites = (productId) => ({
  type: DEL_PRODUCT_FROM_FAVORITES_ACTION_TYPE,
  payload: { productId },
});

export const addToBasket = (productId) => ({
  type: ADD_TO_BASKET_ACTION_TYPE,
  payload: { productId },
});
export const addValueToBasket = (productId) => ({
  type: ADD_TO_BASKET_VALUE_ACTION_TYPE,
  payload: { productId },
});
export const removeProductFromBasket = (productId) => ({
  type: REMOVE_FROM_BASKET_ACTION_TYPE,
  payload: { productId },
});
export const removeValueProductFromBasket = (productId) => ({
  type: REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
  payload: { productId },
});

export const saveFavoriteToLocalStorage = (data) => ({
  type: SAVE_FAVORITES_TO_LOCAL_STORAGE,
  payload: data,
});
export const getFavoriteToLocalStorage = (data) => ({
  type: GET_FAVORITES_LOCAL_STORAGE,
  payload: data,
});
export const saveBasketToLocalStorage = (data) => ({
  type: SAVE_BASKET_TO_LOCAL_STORAGE,
  payload: data,
});
export const getBasketToLocalStorage = (data) => ({
  type: GET_BASKET_LOCAL_STORAGE,
  payload: data,
});
