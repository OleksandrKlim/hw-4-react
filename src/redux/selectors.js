export const favoritesSelector = (state) => state.favorites;
export const basketSelector = (state) => state.basket;
export const productsSelector = (state) => state.products.products;
export const loading = (state) => state.products.isLoading;
export const modalFirst = (state) => state.modal.modalVisibleFirst;
export const modalSecond = (state) => state.modal.modalVisibleSecond;
export const modalThere = (state) => state.modal.modalVisibleThere;
export const selectedId = (state) => state.selectedCard;
