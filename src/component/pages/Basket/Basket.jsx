import React from "react";
import {
  ADD_TO_BASKET_VALUE_ACTION_TYPE,
  REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
  REMOVE_FROM_BASKET_ACTION_TYPE,
  MODAL_THERE_VISIBLE_ACTION_TYPE,
  MODAL_THERE_INVISIBLE_ACTION_TYPE,
  GET_STATE_ARTICLE,
  CLEAR_STATE_ARTICLE,
  delProductFromFavorites,
  addProductToFavorites,
} from "../../../redux/actions";
import {
  favoritesSelector,
  modalThere,
  selectedId,
} from "../../../redux/selectors";
import { useDispatch, useSelector } from "react-redux";

import { Button } from "../../Button/Button";
import { Card } from "../../Card/Card";
import { Modal } from "../../Modal/Modal";
import { Svg } from "../../Button/Svg";
import "./basket.scss";

function Basket() {
  const products = useSelector((state) => state.products.products);
  const favorites = useSelector(favoritesSelector);
  const basket = useSelector((state) => state.basket);
  const modal = useSelector(modalThere);
  const articleCard = useSelector(selectedId);
  const dispatch = useDispatch();
  const addToLike = (productId) => {
    favorites.includes(productId)
      ? dispatch(delProductFromFavorites(productId))
      : dispatch(addProductToFavorites(productId));
  };

  const openModalDeleteProduct = (article) => {
    dispatch({
      type: MODAL_THERE_VISIBLE_ACTION_TYPE,
    });
    dispatch({
      type: GET_STATE_ARTICLE,
      payload: article,
    });
  };

  function incrementBasket(article) {
    dispatch({ type: ADD_TO_BASKET_VALUE_ACTION_TYPE, payload: article });
  }
  const quantity = (productId) => basket[productId];
  function decrementBasket(article) {
    dispatch({
      type: GET_STATE_ARTICLE,
      payload: article,
    });
    basket[article] === 1
      ? dispatch({
          type: MODAL_THERE_VISIBLE_ACTION_TYPE,
        })
      : dispatch({
          type: REMOVE_VALUE_FROM_BASKET_ACTION_TYPE,
          payload: article,
        });
  }
  if (products.length === 0) {
    return null;
  }

  const basketProducts = Object.entries(basket).map(([productId, quantity]) => {
    const product = products.find((product) => {
      return product.article === productId;
    });

    return product;
  });

  return (
    <>
      {Object.keys(basket).length === 0 ? (
        <h2>Корзина порожня. Додайте будьласка товар</h2>
      ) : (
        <ul className="basket">
          {basketProducts.map((el) => (
            <Card
              className="basket__list"
              product={el}
              key={el.article}
              name={el.name}
              img={el.img}
              prise={el.prise}
              color={el.color}
              article={el.article}
              action={
                <>
                  <div>
                    <Button
                      text="+"
                      handleClick={() => incrementBasket(el.article)}
                    />
                    <p className="basket__prise">{quantity(el.article)}</p>
                    <Button
                      text="-"
                      handleClick={() => decrementBasket(el.article)}
                    />
                  </div>
                  <Button
                    class="basket__btn--favorite"
                    text=<Svg
                      color={favorites.includes(el.article) ? "red" : "white"}
                    />
                    handleClick={() => addToLike(el.article)}
                  />

                  <p className=" basket__sum">
                    Вартість: {quantity(el.article) * el.prise}
                  </p>
                  <div
                    onClick={() => {
                      openModalDeleteProduct(el.article);
                    }}
                    className="basket__clothe"
                  />
                </>
              }
            />
          ))}
        </ul>
      )}
      {modal && (
        <Modal
          closeButton={true}
          header="Видалити товар з кошику ?"
          text="Click ok  to delete or cancel if you change your mind "
          wrapperClose={(e) => e.stopPropagation()}
          onClose={() => {
            dispatch({
              type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
            });
          }}
          action={
            <>
              <Button
                text="Ok"
                handleClick={() => {
                  dispatch({
                    type: REMOVE_FROM_BASKET_ACTION_TYPE,
                    payload: articleCard,
                  });
                  dispatch({
                    type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
                  });

                  dispatch({ type: CLEAR_STATE_ARTICLE });
                }}
              />
              <Button
                text="Cancel"
                handleClick={() => {
                  dispatch({
                    type: MODAL_THERE_INVISIBLE_ACTION_TYPE,
                  });
                }}
              />
            </>
          }
        />
      )}
    </>
  );
}

export default Basket;
