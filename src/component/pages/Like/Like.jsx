import React from "react";
import { Card } from "../../Card/Card";

import { Button } from "../../Button/Button";
import "./Like.scss";
import { useDispatch, useSelector } from "react-redux";
import { favoritesSelector } from "../../../redux/selectors";
import { delProductFromFavorites } from "../../../redux/actions";

function Like() {
  const products = useSelector((state) => state.products.products);
  const favorites = useSelector(favoritesSelector);

  const dispatch = useDispatch();

  const productList = products.filter((products) =>
    favorites.includes(products.article)
  );
  const removeLike = (productId) => {
    dispatch(delProductFromFavorites(productId));
  };
  return (
    <>
      {favorites.length === 0 ? (
        <h2>Ви не обрали улюблені товари</h2>
      ) : (
        <ul className="like">
          {productList.map((product) => (
            <Card
              product={product}
              key={product.article}
              name={product.name}
              img={product.img}
              prise={product.prise}
              color={product.color}
              article={product.article}
              action={
                <Button
                  text="Видалити з улюблених"
                  handleClick={() => removeLike(product.article)}
                />
              }
            />
          ))}
        </ul>
      )}
    </>
  );
}

export default Like;
