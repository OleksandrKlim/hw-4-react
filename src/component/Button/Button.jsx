import React from "react";
import PropTypes from "prop-types";
import "./button.scss";
export function Button(props) {
  return (
    <button
      id={props.idButton}
      type="button"
      className={props.class}
      onClick={(e) => {
        props.handleClick();
      }}
    >
      {props.text} {props.context}
    </button>
  );
}

Button.propTypes = {
  style: PropTypes.object,
  onClick: PropTypes.func,
};
