import React from "react";
import PropTypes from "prop-types";
import { Button } from "../Button/Button";
import "./header.scss";
import { NavLink } from "react-router-dom";

import { useSelector } from "react-redux";
import { favoritesSelector } from "../../redux/selectors";

export function Header(props) {
  const favorites = useSelector(favoritesSelector);
  return (
    <header className="header">
      <nav className="header__nav">
        <NavLink
          to="/"
          className={({ isActive }) => (isActive ? "active-link" : "link")}
        >
          Main
        </NavLink>

        <NavLink
          to="/like"
          className={({ isActive }) => (isActive ? "active-link" : "link")}
        >
          Like
        </NavLink>
        <NavLink
          to="/basket"
          className={({ isActive }) => (isActive ? "active-link" : "link")}
        >
          {" "}
          Basket
        </NavLink>
      </nav>
      <div className="header__wrapper">
        <Button
          class="header__button"
          text="Like: "
          context={favorites.length}
        />
        <Button
          class="header__button--buy"
          text="Basket: "
          context={props.basket}
        />
      </div>
    </header>
  );
}

Header.propTypes = {
  text: PropTypes.string,
  class: PropTypes.string,
};
